/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RCC_X2_2_Pin GPIO_PIN_15
#define RCC_X2_2_GPIO_Port GPIOC
#define PVDD_Pin GPIO_PIN_9
#define PVDD_GPIO_Port GPIOF
#define RCC_X1_1_Pin GPIO_PIN_0
#define RCC_X1_1_GPIO_Port GPIOH
#define RCC_X1_2_Pin GPIO_PIN_1
#define RCC_X1_2_GPIO_Port GPIOH
#define M1_CUR_2_Pin GPIO_PIN_0
#define M1_CUR_2_GPIO_Port GPIOC
#define M2_CUR_3_Pin GPIO_PIN_1
#define M2_CUR_3_GPIO_Port GPIOC
#define M2_SPI_SDO_Pin GPIO_PIN_2
#define M2_SPI_SDO_GPIO_Port GPIOC
#define M2_SPI_SDI_Pin GPIO_PIN_3
#define M2_SPI_SDI_GPIO_Port GPIOC
#define M1_ENC_A_Pin GPIO_PIN_0
#define M1_ENC_A_GPIO_Port GPIOA
#define M2_ENC_B_Pin GPIO_PIN_1
#define M2_ENC_B_GPIO_Port GPIOA
#define M2_CUR_1_Pin GPIO_PIN_2
#define M2_CUR_1_GPIO_Port GPIOA
#define M2_CUR_2_Pin GPIO_PIN_3
#define M2_CUR_2_GPIO_Port GPIOA
#define M2_DRV_INHA_Pin GPIO_PIN_5
#define M2_DRV_INHA_GPIO_Port GPIOA
#define M1_CUR_1_Pin GPIO_PIN_6
#define M1_CUR_1_GPIO_Port GPIOA
#define M1_DRV_INHA_Pin GPIO_PIN_7
#define M1_DRV_INHA_GPIO_Port GPIOA
#define M1_CUR_3_Pin GPIO_PIN_4
#define M1_CUR_3_GPIO_Port GPIOC
#define M2_DRV_INHB_Pin GPIO_PIN_0
#define M2_DRV_INHB_GPIO_Port GPIOB
#define M2_DRV_INHC_Pin GPIO_PIN_1
#define M2_DRV_INHC_GPIO_Port GPIOB
#define M1_ENC_Z_Pin GPIO_PIN_2
#define M1_ENC_Z_GPIO_Port GPIOB
#define M2_ENC_Z_Pin GPIO_PIN_11
#define M2_ENC_Z_GPIO_Port GPIOF
#define DIAG_FAULT_Pin GPIO_PIN_14
#define DIAG_FAULT_GPIO_Port GPIOF
#define M1_DRV_INLA_Pin GPIO_PIN_9
#define M1_DRV_INLA_GPIO_Port GPIOE
#define M1_DRV_INHB_Pin GPIO_PIN_10
#define M1_DRV_INHB_GPIO_Port GPIOE
#define M1_DRV_INLB_Pin GPIO_PIN_11
#define M1_DRV_INLB_GPIO_Port GPIOE
#define M1_DRV_INHC_Pin GPIO_PIN_12
#define M1_DRV_INHC_GPIO_Port GPIOE
#define M1_DRV_INLC_Pin GPIO_PIN_13
#define M1_DRV_INLC_GPIO_Port GPIOE
#define M2_I2C_SCL_Pin GPIO_PIN_10
#define M2_I2C_SCL_GPIO_Port GPIOB
#define M2_I2C_SDA_Pin GPIO_PIN_11
#define M2_I2C_SDA_GPIO_Port GPIOB
#define CAN2_RX_Pin GPIO_PIN_12
#define CAN2_RX_GPIO_Port GPIOB
#define CAN2_TX_Pin GPIO_PIN_13
#define CAN2_TX_GPIO_Port GPIOB
#define STLINK_RX_Pin GPIO_PIN_8
#define STLINK_RX_GPIO_Port GPIOD
#define STLINK_TX_Pin GPIO_PIN_9
#define STLINK_TX_GPIO_Port GPIOD
#define M1_SPI_NCS_Pin GPIO_PIN_11
#define M1_SPI_NCS_GPIO_Port GPIOD
#define M2_SPI_NCS_Pin GPIO_PIN_12
#define M2_SPI_NCS_GPIO_Port GPIOD
#define DIAG_LD1_Pin GPIO_PIN_15
#define DIAG_LD1_GPIO_Port GPIOD
#define DIAG_LD2_Pin GPIO_PIN_6
#define DIAG_LD2_GPIO_Port GPIOG
#define DIAG_LD3_Pin GPIO_PIN_7
#define DIAG_LD3_GPIO_Port GPIOG
#define DIAG_LD4_Pin GPIO_PIN_8
#define DIAG_LD4_GPIO_Port GPIOG
#define M2_DRV_INLA_Pin GPIO_PIN_6
#define M2_DRV_INLA_GPIO_Port GPIOC
#define M2_DRV_INLB_Pin GPIO_PIN_7
#define M2_DRV_INLB_GPIO_Port GPIOC
#define M2_DRV_INLC_Pin GPIO_PIN_8
#define M2_DRV_INLC_GPIO_Port GPIOC
#define M2_SPI_SCLK_Pin GPIO_PIN_9
#define M2_SPI_SCLK_GPIO_Port GPIOA
#define CAN1_RX_Pin GPIO_PIN_11
#define CAN1_RX_GPIO_Port GPIOA
#define CAN1_TX_Pin GPIO_PIN_12
#define CAN1_TX_GPIO_Port GPIOA
#define DEBUG_JTSM_Pin GPIO_PIN_13
#define DEBUG_JTSM_GPIO_Port GPIOA
#define DEBUG_JTCK_Pin GPIO_PIN_14
#define DEBUG_JTCK_GPIO_Port GPIOA
#define M2_DRV_EN_GATE_Pin GPIO_PIN_2
#define M2_DRV_EN_GATE_GPIO_Port GPIOD
#define M1_DRV_EN_GATE_Pin GPIO_PIN_3
#define M1_DRV_EN_GATE_GPIO_Port GPIOD
#define M1_DRV_NFAULT_Pin GPIO_PIN_4
#define M1_DRV_NFAULT_GPIO_Port GPIOD
#define M2_DRV_NFAULT_Pin GPIO_PIN_5
#define M2_DRV_NFAULT_GPIO_Port GPIOD
#define M1_SPI_SDO_Pin GPIO_PIN_7
#define M1_SPI_SDO_GPIO_Port GPIOD
#define M1_SPI_SDI_Pin GPIO_PIN_9
#define M1_SPI_SDI_GPIO_Port GPIOG
#define M1_SPI_SCLK_Pin GPIO_PIN_11
#define M1_SPI_SCLK_GPIO_Port GPIOG
#define DEBUG_JTDO_Pin GPIO_PIN_3
#define DEBUG_JTDO_GPIO_Port GPIOB
#define M1_ENC_AB6_Pin GPIO_PIN_6
#define M1_ENC_AB6_GPIO_Port GPIOB
#define M1_ENC_B_Pin GPIO_PIN_7
#define M1_ENC_B_GPIO_Port GPIOB
#define M1_I2C_SCL_Pin GPIO_PIN_8
#define M1_I2C_SCL_GPIO_Port GPIOB
#define M1_I2C_SDA_Pin GPIO_PIN_9
#define M1_I2C_SDA_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
