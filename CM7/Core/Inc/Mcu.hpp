/*
 * Mcu.hpp
 *
 *  Created on: Mar 4, 2024
 *      Author: Adam Czarnecki <ac306237@student.polsl.pl>
 */

#ifndef INC_MCU_H_
#define INC_MCU_H_

#include <main.hpp>



void mcu_SPI_Transmit_and_Receive(SPI_HandleTypeDef* hspi, uint8_t* frame, uint8_t* answer, uint16_t size, uint32_t timeout);
void mcu_delay_ms(uint32_t delay);


#endif /* INC_MCU_H_ */
