/*
 * DriverTMC6200.hpp
 *
 *  Created on: Mar 4, 2024
 *      Author: Adam Czarnecki <ac306237@student.polsl.pl>
 */

#ifndef INC_DRIVERTMC6200_HPP_
#define INC_DRIVERTMC6200_HPP_


#include <Mcu.hpp>



extern SPI_HandleTypeDef hspi1;

class DriverTMC6200 {
public:
	struct frame_bitfield {

		uint32_t data :32;
		uint32_t address :7;
		uint32_t read_write :1;	//MSB

	};
	typedef frame_bitfield frame_bitfield;

	struct reg_gconf {
			uint32_t disable : 1;
			uint32_t singleline : 1;
			uint32_t faultdirect : 1;
			uint32_t unused1 : 1;
			uint32_t amplification : 2;
			uint32_t amplifier_off : 1;
			uint32_t test_mode : 1;
			uint32_t unused2 : 24;
		};
	using reg_gconf = reg_gconf;

		struct reg_gstat{
			uint32_t reset : 1;
			uint32_t drv_otpw : 1;
			uint32_t drv_ot : 1;
			uint32_t uv_cp : 2;
			uint32_t shortdet_u : 1;
			uint32_t s2gu : 1;
			uint32_t s2vsu : 1;
			uint32_t unused1 : 1;
			uint32_t shortdet_v : 1;
			uint32_t s2gv : 1;
			uint32_t s2vsv : 1;
			uint32_t unused2 : 1;
			uint32_t shortdet_w : 1;
			uint32_t s2gw : 1;
			uint32_t s2vsw : 1;

		};
		using reg_gstat = reg_gstat;

		struct reg_ioin {
			uint32_t UL : 1;
			uint32_t UH : 1;
			uint32_t VL : 1;
			uint32_t VH : 1;
			uint32_t WL : 1;
			uint32_t WH : 1;
			uint32_t DRV_EN : 1;
			uint32_t unsend1 : 1;
			uint32_t OTPW : 1;
			uint32_t OT136 : 1;
			uint32_t OT143 : 1;
			uint32_t OT150 : 1;
			uint32_t unsend2 : 12;
			uint32_t VERSION :8;
		};
		using reg_ioin = reg_ioin;

		struct reg_otp_prog {
			uint32_t OTPBIT : 3;
			uint32_t unsend1 : 1;
			uint32_t OTPBYTE : 2;
			uint32_t unsend2 : 2;
			uint32_t OTPMAGIC : 8;
		};
		using reg_otp_prog = reg_otp_prog;

		struct reg_opt_read {
			uint32_t OTP0 : 8;
		};
		using reg_opt_read = reg_opt_read;

		struct reg_factory_conf {
			uint32_t FCLKTRIM : 5;
		};
		using reg_factory_conf = reg_factory_conf;

		struct reg_short_conf {
			uint32_t S2VS_LEVEL : 4;
			uint32_t unsend1 : 4;
			uint32_t S2G_LEVEL : 4;
			uint32_t unsend2 : 4;
			uint32_t SHORTFILTER : 2;
			uint32_t unsend3 : 2;
			uint32_t shortdelay : 1;
			uint32_t unsend4 : 3;
			uint32_t RETRY : 2;
			uint32_t unsend5 : 2;
			uint32_t protect_parallel : 1;
			uint32_t disable_S2G : 1;
			uint32_t disable_S2VS : 2;
		};
		using reg_short_conf = reg_short_conf;

		struct reg_dry_conf {
			uint32_t BBMCLKS : 5;
			uint32_t unsend1 : 11;
			uint32_t OTSELECT : 2;
			uint32_t DRVSTRENGTH : 2;
		};
		using reg_dry_conf = reg_dry_conf;


	union driver_frame {
		frame_bitfield bitfield;
		uint8_t data_tab[5];
		uint64_t frame;
	}typedef driver_frame;

	enum class register_map : uint8_t {
		GCONF = 0X00,		//read and write
		GSTAT = 0x01,		//read and write
		IOIN = 0x04,
		OTP_PROG = 0x06,
		OTP_READ = 0x07,
		FACTORY_CONF = 0x08,
		SHORT_CONF = 0x09,
		DRV_CONF = 0x0A,
		};

	DriverTMC6200();
	virtual ~DriverTMC6200();
	void read_register( register_map address );
	void write_register(register_map address, uint16_t value );
	void interpret_frame(register_map addres);
	void read_diag_registers();
	void read_frame(uint8_t* driver_frame);

		driver_frame outgoing_frame;
		driver_frame incoming_frame;
		int64_t value;

private:
		/* registers, warnings and faults */
		reg_gconf gconf_flags;
		reg_gstat gstat_flags;
		reg_ioin ioin_flags;


};



#endif /* INC_DRIVER_TMC6200_HPP_ */
