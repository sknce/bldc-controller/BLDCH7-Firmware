/*
 * Mcu.cpp
 *
 *  Created on: Mar 4, 2024
 *      Author: Adam Czarnecki <ac306237@student.polsl.pl>
 */

#include <main.hpp>
#include <Mcu.hpp>


/* SPI */
void mcu_SPI_Transmit_and_Receive(SPI_HandleTypeDef* hspi, uint8_t* frame, uint8_t* answer, uint16_t size ,uint32_t timeout) {

	HAL_GPIO_WritePin(M1_SPI_NCS_GPIO_Port, M1_SPI_NCS_Pin, GPIO_PIN_RESET); // chip select
	HAL_SPI_TransmitReceive(hspi, frame, answer, size, timeout);
	//HAL_SPI_Receive(hspi, answer, size, timeout);
	HAL_GPIO_WritePin(M1_SPI_NCS_GPIO_Port, M1_SPI_NCS_Pin, GPIO_PIN_SET);
	HAL_Delay(1); // 500 ns delay (or more) between two frames is required
}
/* COMMON */
void mcu_delay_ms(uint32_t delay) {
	HAL_Delay(delay);
}
