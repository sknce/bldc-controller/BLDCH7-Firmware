/*
 * DriverTMC6200.c
 *
 *  Created on: Mar 4, 2024
 *      Author: Adam Czarnecki <ac306237@student.polsl.pl>
 */


#include <DriverTMC6200.hpp>
#include <Mcu.hpp>
#include <main.hpp>
#include <cstring>

// wartosci rejestrów wypisać w strukturze


DriverTMC6200::DriverTMC6200(){

};

DriverTMC6200::~DriverTMC6200(){

};



void DriverTMC6200::read_register(register_map address) {

	outgoing_frame.frame = 0;
	incoming_frame.frame = 0;
	outgoing_frame.bitfield.read_write = 0;
	outgoing_frame.bitfield.address = (uint8_t)address;
	outgoing_frame.bitfield.data = 0;
	mcu_SPI_Transmit_and_Receive( &hspi1 , outgoing_frame.data_tab, incoming_frame.data_tab, 5 , 10 );
	int64_t value = *incoming_frame.data_tab;
	mcu_delay_ms(100);

}


void DriverTMC6200::write_register(register_map address, uint16_t value ) {

	outgoing_frame.frame = 0;
	incoming_frame.frame = 0;
	outgoing_frame.bitfield.read_write = 1;
	outgoing_frame.bitfield.address = (uint8_t)address;
	outgoing_frame.bitfield.data = value;
	mcu_SPI_Transmit_and_Receive( &hspi1 , outgoing_frame.data_tab, incoming_frame.data_tab, 5 , 10 );

	mcu_delay_ms(100);

}

void DriverTMC6200::interpret_frame(register_map addres) {

	switch((register_map)addres) {
		case register_map::GCONF:
			memcpy(&gconf_flags, &incoming_frame, 4);
			break;
		case register_map::GSTAT:
			memcpy(&gstat_flags, &incoming_frame, 4);
			break;
		case register_map::IOIN:
			memcpy(&ioin_flags, &incoming_frame, 4);
			break;
		case register_map::OTP_PROG:
			break;
		case register_map::OTP_READ:
			break;
		case register_map::FACTORY_CONF:
			break;
		case register_map::SHORT_CONF:
			break;
		case register_map::DRV_CONF:
			break;
		default:
			break;
		}
}

//void DriverTMC6200::read_frame(uint8_t* data) {
//
//	driver_frame frame;
//		frame.data_tab[0] = data[0];
////		frame.data_tab[1] = data[1];
//
//
//    switch((register_map)frame.bitfield.address) {
//    case register_map::GCONF:
//        break;
//    case register_map::GSTAT:
//        break;
//    case register_map::IOIN:
//        break;
//    case register_map::OTP_PROG:
//        break;
//    case register_map::OTP_READ :
//        break;
//    case register_map::FACTORY_CONF :
//        break;
//    case register_map::SHORT_CONF :
//        break;
//    case register_map::DRV_CONF :
//        break;
//    default:
//        break;
//    }
//}


void DriverTMC6200::read_diag_registers() {
	// read registers that contains warning and faults flags
	read_register(register_map::GCONF);
	read_register(register_map::GSTAT);
	read_register(register_map::IOIN);
	read_register(register_map::OTP_PROG);
	read_register(register_map::OTP_READ);
	read_register(register_map::FACTORY_CONF);
	read_register(register_map::SHORT_CONF);
	read_register(register_map::DRV_CONF);
}




